// units.cpp
#include <iostream>
using namespace std;

int main()
{
    int m = 0;
    cout << "Enter a number in meters: ";
    cin >> m;
    cout << m << " m  = " << m / 1000. << " km" << endl;
    cout << m << " dm  = " << m * 10 << "dm" ;
    
	return 0;
}
