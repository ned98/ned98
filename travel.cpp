// fuel.cpp
#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    float tank, eff, price;
    tank = eff = price = 0.;
    cout << "Enter a tank size:" <<endl;
    cin >> tank; 
    cout << "Enter fuel efficiency (km per liter):" <<endl;
    cin >> eff;
    cout << "Enter price per liter:" <<endl;
    cin >> price;
    
    cout << "We can travel " << tank*100/eff << "km" <<endl;
    cout << "For every 100km it will cost " << price*eff << "lv";
	return 0;
}
